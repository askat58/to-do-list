import React from "react";
import "./FormToDo.css";

export default function FormToDo() {
  return (
    <form>
      <input type="text" className="task-input" />
      <button type="submit" placeholder="Add new task">
        <i className="fas fa-plus-square"></i>
      </button>
    </form>
  );
}
