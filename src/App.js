import "./App.css";
import HeaderToDo from "./components/HeaderToDo";
import FormToDo from "./components/FormToDo";
import ListToDo from "./components/ListToDo";

function App() {
  return (
    <div className="App">
      <HeaderToDo />
      <FormToDo />
      <ListToDo />
    </div>
  );
}

export default App;
